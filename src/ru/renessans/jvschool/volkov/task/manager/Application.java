package ru.renessans.jvschool.volkov.task.manager;

import ru.renessans.jvschool.volkov.task.manager.printer.PrintArg;

public class Application {

    public static void main(final String[] args) {
        PrintArg printer = new PrintArg();
        printer.print(args);
    }

}