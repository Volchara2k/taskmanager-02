package ru.renessans.jvschool.volkov.task.manager.constant;

public interface ConstArg {

    String NO_ARG = "Входной аргумент отсутствует!";
    String HELP_ARG = "help";
    String VERSION_ARG = "version";
    String ABOUT_ARG = "about";

    String UNKNOWN_MSG = "Неизвестный аргумент: %s";
    String HELP_MSG =
            String.format("%s - для вывода версии программы; \n" +
                    "%s - для информации о разработчике; \n" +
                    "%s - для вывода списка команд.",
            VERSION_ARG, ABOUT_ARG, HELP_ARG);
    String VERSION_MSG = "Версия: 1.0.0";
    String ABOUT_MSG =
            String.format("%s - разработчик; \n%s - почта.",
            "Valery Volkov", "volkov.valery2013@yandex.ru");

}