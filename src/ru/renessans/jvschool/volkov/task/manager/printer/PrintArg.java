package ru.renessans.jvschool.volkov.task.manager.printer;

import ru.renessans.jvschool.volkov.task.manager.constant.ConstArg;

final public class PrintArg implements ConstArg {

    public void print(final String[] args) {
        if (isEmptyArgs(args)) {
            printMsg(NO_ARG);
            return;
        }

        final String arg = args[0];
        printByArg(arg);
    }

    private boolean isEmptyArgs(final String[] args) {
        return args == null || args.length < 1;
    }

    private void printByArg(final String arg) {
        switch (arg) {
            case HELP_ARG: {
                printMsg(HELP_MSG);
                break;
            }
            case VERSION_ARG: {
                printMsg(VERSION_MSG);
                break;
            }
            case ABOUT_ARG: {
                printMsg(ABOUT_MSG);
                break;
            }
            default: {
                printMsg(String.format(UNKNOWN_MSG, arg));
                break;
            }
        }
    }

    private void printMsg(final String msg) {
        System.out.println(msg);
        System.exit(0);
    }

}